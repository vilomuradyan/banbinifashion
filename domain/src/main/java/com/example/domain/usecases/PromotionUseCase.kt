package com.example.domain.usecases

import com.example.domain.models.promotion.PromotionResponse
import com.example.domain.repositories.PromotionRepository
import kotlinx.coroutines.flow.Flow

typealias PromotionBaseUseCase = BaseUseCase<Unit, Flow<PromotionResponse>>

class PromotionUseCase(
    private val promotionRepository: PromotionRepository
) : PromotionBaseUseCase {
    override suspend fun invoke(params: Unit): Flow<PromotionResponse> = promotionRepository.getPromotion()
}