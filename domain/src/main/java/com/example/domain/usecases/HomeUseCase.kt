package com.example.domain.usecases

import com.example.domain.models.home.HomeResponse
import com.example.domain.repositories.HomeRepository
import kotlinx.coroutines.flow.Flow

typealias HomeBaseUseCase = BaseUseCase<Unit, Flow<HomeResponse>>

class HomeUseCase(
    private val homeRepository: HomeRepository
) : HomeBaseUseCase {
    override suspend fun invoke(params: Unit): Flow<HomeResponse> = homeRepository.getHomePage()
}
