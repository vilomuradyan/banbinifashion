package com.example.domain.models.promotion

data class Highlight(
    val backgroundColor: String,
    val periodicity: Int,
    val textColor: String
)