package com.example.domain.models.home

data class Heading(
    val isHidden: Boolean,
    val text: String
)