package com.example.domain.models.promotion

data class Proline(
    val center: Center,
    val left: String,
    val right: String
)