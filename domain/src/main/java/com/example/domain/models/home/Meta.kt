package com.example.domain.models.home

data class Meta(
    val canonicalUrl: Any,
    val content: Any,
    val description: String,
    val title: String
)