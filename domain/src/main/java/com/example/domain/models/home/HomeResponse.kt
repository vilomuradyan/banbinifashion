package com.example.domain.models.home

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class HomeResponse(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val page: Page
)