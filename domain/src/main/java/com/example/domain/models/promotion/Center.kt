package com.example.domain.models.promotion

data class Center(
    val items: List<Item>
)