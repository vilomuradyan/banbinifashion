package com.example.domain.models.home

data class Page(
    val content: List<Content>,
    val meta: Meta
)