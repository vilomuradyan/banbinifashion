package com.example.domain.models.promotion

data class Content(
    val activeAfter: Any,
    val activeBefore: String,
    val finished: Any
)