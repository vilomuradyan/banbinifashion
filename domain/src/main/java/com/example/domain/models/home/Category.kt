package com.example.domain.models.home

data class Category(
    val backgroundColor: String,
    val image: ImageX,
    val linkUrl: String,
    val title: String
)