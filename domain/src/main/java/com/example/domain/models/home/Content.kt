package com.example.domain.models.home

data class Content(
    val `data`: Data,
    val name: String
)