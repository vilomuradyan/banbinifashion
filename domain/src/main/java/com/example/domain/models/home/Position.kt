package com.example.domain.models.home

data class Position(
    val x: String,
    val y: String
)