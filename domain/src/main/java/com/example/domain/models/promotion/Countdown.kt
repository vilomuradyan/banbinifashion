package com.example.domain.models.promotion

data class Countdown(
    val content: Content,
    val hasGlitch: Boolean,
    val isCountHidden: Boolean,
    val to: String
)