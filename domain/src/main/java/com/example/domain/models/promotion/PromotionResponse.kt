package com.example.domain.models.promotion

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class PromotionResponse(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val user: User
)