package com.example.domain.models.promotion

data class Item(
    val content: String,
    val countdown: Countdown,
    val duration: Int,
    val highlight: Highlight?
)