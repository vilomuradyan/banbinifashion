package com.example.domain.models.home

data class Cta(
    val backgroundColor: String,
    val text: Any,
    val textColor: String
)