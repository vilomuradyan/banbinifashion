package com.example.domain.models.home

data class Caption(
    val cta: Cta,
    val description: Any,
    val heading: Heading,
    val isInverted: Boolean,
    val position: Position
)