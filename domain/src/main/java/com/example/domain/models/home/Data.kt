package com.example.domain.models.home

data class Data(
    val caption: Caption,
    val categories: List<Category>?,
    val image: ImageX?,
    val isMainImageRight: Boolean,
    val linkUrl: String,
    val size: String,
    val title: String?,
    val video: Video
)