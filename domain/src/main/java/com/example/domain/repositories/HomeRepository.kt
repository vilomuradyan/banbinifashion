package com.example.domain.repositories

import com.example.domain.models.home.HomeResponse
import kotlinx.coroutines.flow.Flow

interface HomeRepository {
    suspend fun getHomePage(): Flow<HomeResponse>
}