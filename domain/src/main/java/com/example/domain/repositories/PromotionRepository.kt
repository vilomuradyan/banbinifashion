package com.example.domain.repositories

import com.example.domain.models.promotion.PromotionResponse
import kotlinx.coroutines.flow.Flow

interface PromotionRepository {
    suspend fun getPromotion(): Flow<PromotionResponse>
}