package com.example.banbinifashion.features.viewHolders

import androidx.recyclerview.widget.RecyclerView
import com.example.banbinifashion.databinding.ItemCategoryBinding
 import com.example.domain.models.home.Category

class CategoryViewHolder(val binding: ItemCategoryBinding) : RecyclerView.ViewHolder(binding.root) {
    fun onBind(data: Category) {
        binding.data = data
    }
}