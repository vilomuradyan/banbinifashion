package com.example.banbinifashion.features.viewHolders

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.banbinifashion.databinding.ItemCategoriesBinding
import com.example.banbinifashion.features.adapters.CategoriesAdapter
import com.example.banbinifashion.models.HomePage

class CategoriesViewHolder(val binding: ItemCategoriesBinding) : RecyclerView.ViewHolder(binding.root) {

    fun onBind(viewPool: RecyclerView.RecycledViewPool, data: HomePage.Categories) {

        binding.list.run {
            layoutManager = GridLayoutManager(binding.root.context, 2)
            setRecycledViewPool(viewPool)
            adapter = CategoriesAdapter().apply {
                updateData(data.category)
            }
        }
    }
}