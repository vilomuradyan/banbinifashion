package com.example.banbinifashion.features.home

import android.view.View
import androidx.lifecycle.lifecycleScope
import com.example.banbinifashion.R
import com.example.banbinifashion.base.BaseFragment
import com.example.banbinifashion.base.BaseViewModel
import com.example.banbinifashion.databinding.FragmentHomeBinding
import com.example.banbinifashion.features.adapters.HomePageAdapter
import com.example.banbinifashion.viewModels.HomeViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : BaseFragment<FragmentHomeBinding>(R.layout.fragment_home) {

    private val mViewModel by viewModel<HomeViewModel>()

    override fun getViewModel(): BaseViewModel = mViewModel

    override fun onResume() {
        super.onResume()
        fetchData()
        lifecycleScope.launch {
            mViewModel.getData()
        }
    }

    private fun fetchData() {
        lifecycleScope.launch {
            mViewModel.response.collectLatest {
                if (it.homePage.isEmpty() && it.promotion.isEmpty()) {
                    mBinding.labelEmptyPage.visibility = View.VISIBLE
                } else {
                    mBinding.list.run {
                        adapter = HomePageAdapter().apply {
                            updateData(it.homePage)
                        }
                    }

                    it.promotion.forEach { promotion ->
                        mBinding.promotion = promotion
                        delay(promotion.duration.toLong())
                    }
                }
            }
        }
    }
}