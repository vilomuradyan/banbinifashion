package com.example.banbinifashion.features.viewHolders

import androidx.recyclerview.widget.RecyclerView
import com.example.banbinifashion.databinding.ItemHeaderBinding
import com.example.banbinifashion.models.HomePage

class HeaderViewHolder(val binding: ItemHeaderBinding) : RecyclerView.ViewHolder(binding.root) {
    fun onBind(data: HomePage.Header) {
        binding.headerData = data
    }
}