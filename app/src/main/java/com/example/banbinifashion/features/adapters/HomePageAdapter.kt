package com.example.banbinifashion.features.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.banbinifashion.databinding.ItemCategoriesBinding
import com.example.banbinifashion.databinding.ItemHeaderBinding
import com.example.banbinifashion.features.viewHolders.CategoriesViewHolder
import com.example.banbinifashion.features.viewHolders.HeaderViewHolder
import com.example.banbinifashion.models.HomePage

class HomePageAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val mItems = mutableListOf<HomePage>()
    private val viewPool = RecyclerView.RecycledViewPool()

    /**
     * it is not good solution it would be better use DiffUtil or ListAdapter
     */

    @SuppressLint("NotifyDataSetChanged")
    fun updateData(data: List<HomePage>) {
        mItems.clear()
        mItems.addAll(data)
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return when (mItems[position]) {
            is HomePage.Header -> 0
            is HomePage.Categories -> 1
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            0 -> {
                HeaderViewHolder(
                    ItemHeaderBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                )
            }
            else -> {
                CategoriesViewHolder(
                    ItemCategoriesBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                )
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is HeaderViewHolder -> {
                holder.onBind(mItems[position] as HomePage.Header)
            }
            is CategoriesViewHolder -> {
                holder.onBind(viewPool, mItems[position] as HomePage.Categories)
            }
        }
    }

    override fun getItemCount(): Int = mItems.size
}