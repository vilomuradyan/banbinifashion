package com.example.banbinifashion.features.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.banbinifashion.databinding.ItemCategoryBinding
import com.example.banbinifashion.features.viewHolders.CategoryViewHolder
import com.example.domain.models.home.Category

class CategoriesAdapter : RecyclerView.Adapter<CategoryViewHolder>() {

    private val mItems = mutableListOf<Category>()

    /**
     * it is not good solution it would be better use DiffUtil or ListAdapter
     */

    @SuppressLint("NotifyDataSetChanged")
    fun updateData(data: MutableList<Category>) {
        mItems.clear()
        mItems.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder =
        CategoryViewHolder(
            ItemCategoryBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        holder.onBind(mItems[position])
    }

    override fun getItemCount(): Int = mItems.size
}