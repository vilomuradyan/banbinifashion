package com.example.banbinifashion.features.mapper

import com.example.banbinifashion.models.HomePage
import com.example.banbinifashion.models.Promotion

data class FashionData(
    val promotion: MutableList<Promotion>,
    val homePage: MutableList<HomePage>
)