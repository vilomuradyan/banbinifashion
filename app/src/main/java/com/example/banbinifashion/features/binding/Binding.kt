package com.example.banbinifashion.features.binding

import android.graphics.Color
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import coil.load

object Binding {

    @BindingAdapter("app:loadImage", "app:imageBackground", requireAll = false)
    @JvmStatic
    fun loadImage(imageView: ImageView, url: String, backGround: String?) {
        imageView.load(url)
        backGround?.let {
            imageView.setBackgroundColor(Color.parseColor(it))
        }
    }

    @BindingAdapter("app:title")
    @JvmStatic
    fun setTitle(textView: TextView, title: String?) {
        textView.text = title
    }

    @BindingAdapter("app:promotionBackground")
    @JvmStatic
    fun changeBackground(textView: TextView, background: String?) {
        background?.let {
            textView.setBackgroundColor(Color.parseColor(it))
        }
    }

    @BindingAdapter("app:promotionTextColor")
    @JvmStatic
    fun changeTextColor(textView: TextView, color: String?) {
        color?.let {
            textView.setTextColor(Color.parseColor(it))
        }
    }

    @BindingAdapter("app:promotionText")
    @JvmStatic
    fun changeText(textView: TextView, text: String?) {
        text?.let {
            val data = it.replace("<strong>","")
                .replace("</strong>","")
                .replace("<strong dir>","")
                .replace("<strong dir=\"ltr\">","")
                .replace("|","")
            textView.text = data
        }
    }
}