package com.example.banbinifashion.models

data class Promotion(
    val duration: Int,
    val content: String,
    val backgroundColor: String?,
    val textColor: String?
)
