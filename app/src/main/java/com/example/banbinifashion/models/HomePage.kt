package com.example.banbinifashion.models

import com.example.domain.models.home.Category

sealed class HomePage {
    data class Header(val title: String?, val image: String) : HomePage()
    data class Categories(val category: MutableList<Category>) : HomePage()
}
