package com.example.banbinifashion.di

import com.example.local.database.AppDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val localDataModule = module {
    factory { AppDatabase.provideAppDataBase(androidContext()).homeResponseDao() }
    factory { AppDatabase.provideAppDataBase(androidContext()).promotionResponseDao() }
}