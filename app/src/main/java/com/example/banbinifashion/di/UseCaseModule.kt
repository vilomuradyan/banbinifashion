package com.example.banbinifashion.di

import com.example.domain.repositories.HomeRepository
import com.example.domain.repositories.PromotionRepository
import com.example.domain.usecases.HomeBaseUseCase
import com.example.domain.usecases.HomeUseCase
import com.example.domain.usecases.PromotionBaseUseCase
import com.example.domain.usecases.PromotionUseCase
import org.koin.core.qualifier.named
import org.koin.dsl.module

val useCaseModule = module {

    factory(named("home")) { provideHomeUseCase(get()) }
    factory(named("promotion")) { providePromotionUseCase(get()) }
}

private fun provideHomeUseCase(homeRepository: HomeRepository): HomeBaseUseCase = HomeUseCase(homeRepository)
private fun providePromotionUseCase(promotionRepository: PromotionRepository): PromotionBaseUseCase = PromotionUseCase(promotionRepository)