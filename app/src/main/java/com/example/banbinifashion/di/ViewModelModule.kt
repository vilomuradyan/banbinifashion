package com.example.banbinifashion.di

import com.example.banbinifashion.viewModels.HomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

val viewModelModule = module {
    viewModel {
        HomeViewModel(
            get(named("home")),
            get(named("promotion")),
            get(), get())
    }
}