package com.example.banbinifashion.di

import com.example.banbinifashion.BuildConfig
import com.example.remote.api.FashionApiService
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val networkModule = module {

    single { provideApiService(retrofit = get()) }
    single { provideRetrofit(okHttpClient = get(), url = BuildConfig.API_KEY) }
    single { provideOkHttpClient() }
}

internal fun provideOkHttpClient(): OkHttpClient {
    val httpLoggingInterceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }
    return OkHttpClient.Builder()
        .connectTimeout(60L, TimeUnit.SECONDS)
        .readTimeout(60L, TimeUnit.SECONDS)
        .addNetworkInterceptor { chain ->
            val requestBuilder: Request.Builder = chain.request().newBuilder()

            /**
             * using header directly is not a good solution it would be much better to use encryption
             */

            requestBuilder.addHeader("bf-api-key", "oMXmKN4YSgD8RgeFfMOF54FdyENIxp")
            requestBuilder.addHeader("bf-localization", "AT,USD,en")
            chain.proceed(requestBuilder.build())
        }
        .addInterceptor(httpLoggingInterceptor)
        .build()
}

internal fun provideRetrofit(okHttpClient: OkHttpClient, url: String): Retrofit {
    return Retrofit.Builder()
        .baseUrl(url)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}

internal fun provideApiService(retrofit: Retrofit): FashionApiService =
    retrofit.create(FashionApiService::class.java)
