package com.example.banbinifashion.di

import com.example.domain.repositories.HomeRepository
import com.example.domain.repositories.PromotionRepository
import com.example.remote.repositories.HomeRepositoryImpl
import com.example.remote.repositories.PromotionRepositoryImpl
import org.koin.dsl.module

val remoteDataModule = module {
    factory<HomeRepository> { HomeRepositoryImpl(get(), get()) }
    factory<PromotionRepository> { PromotionRepositoryImpl(get(), get()) }
}