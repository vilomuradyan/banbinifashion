package com.example.banbinifashion.viewModels

import androidx.lifecycle.viewModelScope
import com.example.banbinifashion.base.BaseViewModel
import com.example.banbinifashion.features.mapper.FashionData
import com.example.banbinifashion.models.HomePage
import com.example.banbinifashion.models.Promotion
import com.example.domain.models.home.HomeResponse
import com.example.domain.models.promotion.PromotionResponse
import com.example.domain.usecases.HomeBaseUseCase
import com.example.domain.usecases.PromotionBaseUseCase
import com.example.local.dao.HomePageDao
import com.example.local.dao.PromotionDao
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.zip
import kotlinx.coroutines.launch

class HomeViewModel(
    private val homeBaseUseCase: HomeBaseUseCase,
    private val promotionBaseUseCase: PromotionBaseUseCase,
    private val homePageDao: HomePageDao,
    private val promotionDao: PromotionDao
) : BaseViewModel() {

    private val _response: MutableSharedFlow<FashionData> = MutableSharedFlow(replay = 1, onBufferOverflow = BufferOverflow.DROP_OLDEST)
    val response: SharedFlow<FashionData> get() = _response

    suspend fun getData() = viewModelScope.launch {
        homeBaseUseCase.invoke(Unit).zip(promotionBaseUseCase.invoke(Unit)) { homeResponse, promotionResponse ->
            val homePage = mappingHomePage(homeResponse)
            val promotion = mappingPromotion(promotionResponse)
            FashionData(promotion = promotion, homePage = homePage)
        }.catch {
            val homePage = mappingHomePage(homePageDao.getHomeResponse())
            val promotion = mappingPromotion(promotionDao.getPromotionResponse())
            _response.tryEmit(FashionData(promotion = promotion, homePage = homePage))
        }.collectLatest {
            _response.tryEmit(it)
        }
    }

    private fun mappingHomePage(homeResponse: HomeResponse?): MutableList<HomePage> {
        val homePage = mutableListOf<HomePage>()
        homeResponse?.page?.content?.forEach { content ->
            content.data.image?.let { image ->
                homePage.add(HomePage.Header(
                    title = content.data.title,
                    image = image.src
                ))
            }
            content.data.categories?.let {
                homePage.add(HomePage.Categories(it.toMutableList()))
            }
        }
        return homePage
    }

    private fun mappingPromotion(promotionResponse: PromotionResponse?): MutableList<Promotion> {
        val promotion = mutableListOf<Promotion>()
        promotionResponse?.user?.proline?.center?.items?.forEach {
            promotion.add(Promotion(
                duration = it.duration,
                content = it.content,
                backgroundColor = it.highlight?.backgroundColor,
                textColor = it.highlight?.textColor
            ))
        }

        return promotion
    }
}