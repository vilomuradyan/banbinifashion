package com.example.banbinifashion

import android.app.Application
import com.example.banbinifashion.di.localDataModule
import com.example.banbinifashion.di.networkModule
import com.example.banbinifashion.di.remoteDataModule
import com.example.banbinifashion.di.useCaseModule
import com.example.banbinifashion.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.GlobalContext.startKoin
import org.koin.core.logger.Level


class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger(Level.ERROR)
            androidContext(this@App)
            modules(
                networkModule,
                viewModelModule,
                remoteDataModule,
                useCaseModule,
                localDataModule
            )
        }
    }
}
