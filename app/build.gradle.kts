import org.jetbrains.kotlin.konan.properties.Properties

plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("kapt")
    id("androidx.navigation.safeargs")
}

android {
    compileSdk = ConfigData.compileSdkVersion
    buildToolsVersion = ConfigData.buildToolsVersion

    defaultConfig {
        minSdk = ConfigData.minSdkVersion
        targetSdk = ConfigData.targetSdkVersion

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }

        val properties = Properties()
        properties.load(project.rootProject.file("local.properties").inputStream())
        buildConfigField("String", "API_KEY", "\"${properties.getProperty("API_KEY")}\"")
    }

    buildTypes {
        release {
            isMinifyEnabled = true
            isShrinkResources = true
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }

    buildFeatures {
        dataBinding = true
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = "1.8"
    }
    packagingOptions {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }
}

dependencies {

    // Androidx dependencies
    implementation(Dependencies.lifecycle)
    implementation(Dependencies.core)
    implementation(Dependencies.appCompat)
    implementation(Dependencies.material)

    // Coroutine Dependencies
    implementation(Dependencies.coroutinesCore)
    implementation(Dependencies.coroutinesAndroid)

    // Koin Dependencies
    implementation(Dependencies.koinNavigation)
    implementation(Dependencies.koinAndroid)
    implementation(Dependencies.koinCore)

    // Navigation Dependencies
    implementation(Dependencies.navigation)
    implementation(Dependencies.navigationUi)

    // Junit test dependencies
    testImplementation(TestDependencies.junit)

    // Coroutine Flow unit test dependency
    testImplementation(TestDependencies.turbine)

    // Network Dependencies
    implementation(Dependencies.retrofit)
    implementation(Dependencies.converter)
    implementation(Dependencies.interceptor)

    // Truth google dependency
    testImplementation(TestDependencies.truth)

    // Coroutine test dependency
    testImplementation(TestDependencies.coroutine)

    // Junit android dependencies
    androidTestImplementation(AndroidTestDependencies.junit)

    // Espresso dependencies
    androidTestImplementation(AndroidTestDependencies.espresso)

    // ROOM DB
    implementation(Dependencies.room)
    kapt(Dependencies.roomCompiler)

    // Coil
    implementation(Dependencies.coil)

    // Modules Dependencies
    implementation(project(Modules.local))
    implementation(project(Modules.remote))
    implementation(project(Modules.domain))
}

kapt {
    correctErrorTypes = true
}