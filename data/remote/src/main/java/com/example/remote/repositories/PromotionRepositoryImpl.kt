package com.example.remote.repositories

import com.example.domain.models.promotion.PromotionResponse
import com.example.domain.repositories.PromotionRepository
import com.example.local.dao.PromotionDao
import com.example.remote.api.FashionApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class PromotionRepositoryImpl(
    private val fashionApiService: FashionApiService,
    private val promotionDao: PromotionDao
) : PromotionRepository {
    override suspend fun getPromotion(): Flow<PromotionResponse> = flow {
        promotionDao.insertPromotionResponse(fashionApiService.getPromotion())
        emit(promotionDao.getPromotionResponse())
    }.flowOn(Dispatchers.IO)
}