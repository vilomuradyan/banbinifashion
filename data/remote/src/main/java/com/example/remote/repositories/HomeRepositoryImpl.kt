package com.example.remote.repositories

import com.example.domain.models.home.HomeResponse
import com.example.domain.repositories.HomeRepository
import com.example.local.dao.HomePageDao
import com.example.remote.api.FashionApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class HomeRepositoryImpl(
    private val fashionApiService: FashionApiService,
    private val homePageDao: HomePageDao
) : HomeRepository {
    override suspend fun getHomePage(): Flow<HomeResponse> = flow {
        homePageDao.insertHomeResponse(fashionApiService.getHome())
        emit(homePageDao.getHomeResponse())
    }.flowOn(Dispatchers.IO)
}