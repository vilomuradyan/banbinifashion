package com.example.remote.api

import com.example.domain.models.home.HomeResponse
import com.example.domain.models.promotion.PromotionResponse
import retrofit2.http.GET

interface FashionApiService {

    @GET("./user:proline")
    suspend fun getPromotion(): PromotionResponse

    @GET("./page:type=landing")
    suspend fun getHome(): HomeResponse
}