package com.example.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.domain.models.home.HomeResponse

@Dao
interface HomePageDao {

    @Query("SELECT * FROM HomeResponse")
    suspend fun getHomeResponse(): HomeResponse

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertHomeResponse(homeResponse: HomeResponse)
}