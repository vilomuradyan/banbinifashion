package com.example.local.converters

import androidx.room.TypeConverter
import com.example.domain.models.home.Page
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

object HomeConverter {

    @TypeConverter
    @JvmStatic
    fun stringToMap(value: String): Page {
        return Gson().fromJson(value, object : TypeToken<Page>() {}.type)
    }

    @TypeConverter
    @JvmStatic
    fun mapToString(value: Page?): String {
        return if (value == null) "" else Gson().toJson(value)
    }
}