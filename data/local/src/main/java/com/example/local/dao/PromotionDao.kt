package com.example.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.domain.models.promotion.PromotionResponse

@Dao
interface PromotionDao {

    @Query("SELECT * FROM PromotionResponse")
    suspend fun getPromotionResponse(): PromotionResponse

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPromotionResponse(promotionResponse: PromotionResponse)
}