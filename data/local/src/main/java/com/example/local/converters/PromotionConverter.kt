package com.example.local.converters

import androidx.room.TypeConverter
import com.example.domain.models.promotion.User
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

object PromotionConverter {

    @TypeConverter
    @JvmStatic
    fun stringToMap(value: String): User {
        return Gson().fromJson(value, object : TypeToken<User>() {}.type)
    }

    @TypeConverter
    @JvmStatic
    fun mapToString(value: User?): String {
        return if (value == null) "" else Gson().toJson(value)
    }
}