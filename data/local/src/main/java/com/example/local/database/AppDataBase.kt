package com.example.local.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.domain.models.home.HomeResponse
import com.example.domain.models.promotion.PromotionResponse
import com.example.local.converters.HomeConverter
import com.example.local.converters.PromotionConverter
import com.example.local.dao.HomePageDao
import com.example.local.dao.PromotionDao

@TypeConverters(HomeConverter::class, PromotionConverter::class)
@Database(entities = [HomeResponse::class, PromotionResponse::class], version = 4, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun homeResponseDao(): HomePageDao
    abstract fun promotionResponseDao() : PromotionDao

    companion object {

        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun provideAppDataBase(context: Context): AppDatabase {
            synchronized(this) {
                var instance = INSTANCE
                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java, "fashion_db"
                    )
                        .fallbackToDestructiveMigration()
                        .build()
                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}