object Versions {

    const val gradlePlugin = "7.1.3"
    const val kotlin = "1.5.21"
    const val jUnit = "4.13.2"
    const val lifecycle = "2.5.1"
    const val androidJunit = "1.1.3"
    const val espresso = "3.4.0"
    const val core = "1.7.0"
    const val turbine = "0.8.0"
    const val truth = "1.1"
    const val coroutines = "1.6.4"
    const val appCompat = "1.5.0"
    const val material = "1.6.1"
    const val retrofit = "2.9.0"
    const val interceptor = "4.10.0"
    const val koin = "3.2.0"
    const val navigation = "2.5.1"
    const val room = "2.4.3"
    const val coil = "2.2.0"
    const val gson = "2.9.0"

}