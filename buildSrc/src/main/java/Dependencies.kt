/**
 * To define plugins
 */
object BuildPlugins {
    val android by lazy { "com.android.tools.build:gradle:${Versions.gradlePlugin}" }
    val kotlin by lazy { "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}" }
}

/**
 * To define dependencies
 */
object Dependencies {

    // Androidx Dependencies
    val lifecycle by lazy { "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.lifecycle}" }
    val core by lazy { "androidx.core:core-ktx:${Versions.core}" }
    val appCompat by lazy { "androidx.appcompat:appcompat:${Versions.appCompat}" }

    // Google Dependencies
    val material by lazy { "com.google.android.material:material:${Versions.material}" }
    val gson by lazy { "com.google.code.gson:gson:${Versions.gson}" }

    // Coroutine Dependencies
    val coroutinesCore by lazy { "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.coroutines}" }
    val coroutinesAndroid by lazy { "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutines}" }

    // Network Dependencies
    val retrofit by lazy { "com.squareup.retrofit2:retrofit:${Versions.retrofit}" }
    val converter by lazy { "com.squareup.retrofit2:converter-gson:${Versions.retrofit}" }
    val interceptor by lazy { "com.squareup.okhttp3:logging-interceptor:${Versions.interceptor}" }

    // Koin Dependencies
    val koinCore by lazy { "io.insert-koin:koin-core:${Versions.koin}" }
    val koinAndroid by lazy { "io.insert-koin:koin-android:${Versions.koin}" }
    val koinNavigation by lazy { "io.insert-koin:koin-androidx-navigation:${Versions.koin}" }

    // Navigation Dependencies
    val navigation by lazy { "androidx.navigation:navigation-fragment-ktx:${Versions.navigation}" }
    val navigationUi by lazy { "androidx.navigation:navigation-ui-ktx:${Versions.navigation}" }

    // DataBase Dependencies
    val room by lazy { "androidx.room:room-ktx:${Versions.room}" }
    val roomCompiler by lazy { "androidx.room:room-compiler:${Versions.room}" }

    // Image Dependencies
    val coil by lazy { "io.coil-kt:coil:${Versions.coil}" }
}

/**
 * To define Test dependencies
 */
object TestDependencies {

    // Junit dependencies
    val junit by lazy { "junit:junit:${Versions.jUnit}" }

    // Turbine Dependency
    val turbine by lazy { "app.cash.turbine:turbine:${Versions.turbine}" }

    // Google Dependencies
    val truth by lazy { "com.google.truth:truth:${Versions.truth}" }

    // Coroutine Dependencies
    val coroutine by lazy { "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.coroutines}" }
}

/**
 * To define android test dependencies
 */
object AndroidTestDependencies {

    val junit by lazy { "androidx.test.ext:junit:${Versions.androidJunit}" }
    val espresso by lazy { "androidx.test.espresso:espresso-core:${Versions.espresso}" }
}

object Modules {

    val local by lazy { ":data:local" }
    val remote by lazy { ":data:remote" }
    val domain by lazy { ":domain" }
}